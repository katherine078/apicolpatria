﻿using ApiBanco.Data.Interfaces;
using ApiBanco.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ApiBanco.Data.Repository
{
    public class Banco :  BaseRepository, IBanco
    {
        public Banco (string CnnString) : base(CnnString)
        {
        }
        public bool InsertarUsuIniciarsesionario(Iniciarsesion a)
        {

            IDbCommand dbTransation;
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@user", a.user);
            parameters.Add("@pass", a.pass);

            using (IDbConnection db = GetConnection())
            {
                db.Open();

                try
                {
                    string result = null;
                    object value = db.ExecuteScalar("Usuario.Iniciarsesion",
                        commandType: CommandType.StoredProcedure,
                        param: parameters);

                    if (value != null)
                    {
                        result = value.ToString();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                    db.Close();
                }
                catch (Exception e)
                {
                    db.Close();
                    return false;
                    throw e;
                }
            }
        }
    }
}
