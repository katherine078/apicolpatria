﻿using ApiBanco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiBanco.Data.Interfaces
{
   public interface IBanco
    {
        bool InsertarUsuIniciarsesionario(Iniciarsesion a);
    }
}
