﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiBanco.Models
{
    public class Iniciarsesion
    {
        public string user { get; set; }
        public string pass { get; set; }
        public string ingreso { get; set; }
    }
    public class Props
    {
        public string type { get; set; }
        public int totalOverdraft { get; set; }
        public int? availableOverdraft { get; set; }
        public int? debtOverdraft { get; set; }
        public string nextPaymentDate { get; set; }
        public string nextPaymentAmount { get; set; }
    }
    [System.Serializable]
    public class Account
    {
        public string id { get; set; }
        public string name { get; set; }
        public double amount { get; set; }
        public int status { get; set; }
        public string openDate { get; set; }
        public Props props { get; set; }
    }

    public class CreditCard
    {
        public string name { get; set; }
        public object number { get; set; }
        public string openDate { get; set; }
        public int balance { get; set; }
        public int availableBalance { get; set; }
        public int debt { get; set; }
        public int status { get; set; }
        public Props props { get; set; }
    }

    public class Loans
    {
        public string name { get; set; }
        public int amount { get; set; }
        public long number { get; set; }
        public string openDate { get; set; }
        public int months { get; set; }
        public int status { get; set; }
        public int debtMonts { get; set; }
        public Props props { get; set; }
    }

    public class Datap
    {
        public List<Account> accounts { get; set; }
        public List<CreditCard> creditCard { get; set; }
        public Loans loans { get; set; }
    }

    public class Root
    {
        public Datap data { get; set; }
    }


}
