﻿using ApiBanco.Models;
using Microsoft.AspNetCore.Http;
using ApiBanco.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiBanco.Data;

namespace ApiBanco.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BancoController : ControllerBase
    {
        public readonly IBanco al;
        public readonly Jsonbanco json;


        public BancoController(IBanco al)
        {
            this.al = al;
        }
      
        [HttpPost]
        [Route("iniciarSesion")]
        public IActionResult Iniciarsesion([FromBody] Iniciarsesion a)
        {
            try
            {
                return Ok(al.InsertarUsuIniciarsesionario(a));

            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        [HttpGet]
        [Route("Lst_Datos")]
        public async Task<IActionResult> GetDatos()
        
        
        {
            try
            {
                Jsonbanco js = new Jsonbanco();
                return Ok(js.LeerJson());

            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }




    }
}
